import lxml.etree
import lxml.html
import requests
import re
import json
import os
from urllib.request import unquote
from proxy_scraper import Proxy
import time
import click


@click.command()
@click.option('--search', '-f', default='', help='name of requests')

def main(search):
    S = Scraper("https://www.yelp.com", search)
    S.start()

# TODO: link, firm, phones, site, description, areas_of_law, lawyers, lat, lon, country, state, city,
#  partner_type "visa service firm", active true


class Scraper:
    def __init__(self, base, search_text):
        self.s = requests.Session()
        self.base = base
        self.headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36'}
        self.s.headers = self.headers

        self.search_text = search_text

        self.dump_path = 'dumps'
        self.locations_dump = os.path.join(self.dump_path, 'locations.json')
        if not os.path.exists(self.dump_path):
            os.makedirs(self.dump_path)

        self.dump_items_links_path = os.path.join(self.dump_path, 'items_links')
        if not os.path.exists(self.dump_items_links_path):
            os.makedirs(self.dump_items_links_path)

        self.dump_items_path = os.path.join(self.dump_path, 'items')
        if not os.path.exists(self.dump_items_path):
            os.makedirs(self.dump_items_path)

        self.items_links = list()
        self.items = list()

        self.proxy = Proxy()
        self.current_proxy = ''

    def send_req(self, url, **kwargs):
        while True:
            try:
                if not self.current_proxy:
                    raise Exception('Proxy is blank')
                print('request:', url, kwargs)
                r = requests.get(url,
                                 params=kwargs,
                                 proxies={'https': self.current_proxy},
                                 timeout=15)
                if 'Sorry, you’re not allowed to access this page.' in r.text:
                    raise Exception('Sorry, you’re not allowed to access this page')
                if 'Before you continue, we just need to check that you' in r.text:
                    raise Exception('Captcha detected')
                if '<meta name="ROBOTS"' in r.text:
                    raise Exception('Robot detected')
                return r
            except Exception as ex:
                print(f'Error: {ex}\n')
                while True:
                    try:
                        self.current_proxy = self.proxy.get_fresh_proxy()
                        print(f'rotate IP to {self.current_proxy}')
                        r = requests.get('https://ipinfo.info/html/ip_checker.php',
                                         proxies={'https': self.current_proxy},
                                         timeout=15)
                        root = lxml.html.fromstring(r.content)
                        print(root.xpath('//a[@title="My IP Address"]/b/text()'))
                        break
                    except Exception as ex2:
                        print(f'Bad proxy {ex2} \n')

    def send_req2(self, url, **kwargs):
        while True:
            try:
                r = requests.get(url, params=kwargs)
                if 'Sorry, you’re not allowed to access this page.' in r.text:
                    raise Exception('Sorry, you’re not allowed to access this page')
                if 'Before you continue, we just need to check that you' in r.text:
                    raise Exception('Robot detected')
                return r
            except Exception as ex:
                print(f'Error: {ex}\n')
                time.sleep(30)

    def scrap_items_links(self, r):
        root = lxml.html.fromstring(r.content)
        found_links = list(
            set(filter(lambda x: not '?hrid=' in x, root.xpath('//a[starts-with(@href, "/biz")]/@href'))))
        return found_links

    def search_items_links(self, state, city, **params):
        params['find_desc'] = self.search_text
        params['find_loc'] = f'{state}, {city}'
        r = self.send_req('https://www.yelp.com/search', **params)

        with open('test1.html', 'wb') as f:
            f.write(r.content)

        found_links = self.scrap_items_links(r)
        if not found_links:
            return []

        self.items_links.extend(found_links)
        pages = re.findall(r'Showing (\d+)-(\d+) of (\d+)', r.text)[0]
        if pages[1] == pages[2]:
            return found_links

        params['start'] = pages[1]
        found_links.extend(self.search_items_links(state, city, **params))
        return list(set(found_links))

    def load_items_links(self, state, city):
        self.items_links = list()
        dump_path = os.path.join(self.dump_items_links_path, f'{state}_{city}.json'.replace(' ', '-'))
        if os.path.exists(dump_path):
            self.items_links = json.load(open(dump_path, 'r'))
        if not self.items_links:
            print(f'load items links {state} {city}')
            self.search_items_links(state, city)
            json.dump(self.items_links, open(dump_path, 'w'), indent=4)

    def get_hours(self, root):
        schedule = dict()
        hours_table_rows = root.xpath('//table[@class="table table-simple hours-table"]/tbody/tr')

        for row in hours_table_rows:
            day = row.xpath('th/text()')
            if not day:
                continue
            day = day[0]
            schedule[day] = ' - '.join(filter(lambda x: x,
                                              [x.strip() for x in
                                               row.xpath('td//span/text()')[:2]]))
            if not schedule[day]:
                schedule[day] = 'Closed'

        return schedule

    def get_first(self, root, xpath):
        target = root.xpath(xpath)
        if target:
            return target[0].strip()
        else:
            return ''

    def get_website(self, root):
        redirect = root.xpath('//span[contains(@class, "biz-website")]/a/@href')
        if not redirect:
            return ''
        decoded = unquote(redirect[0])
        return re.findall(r'url=(\S+?)&', decoded)[0]

    def get_location(self, r):
        out = dict()
        out['point'] = re.findall(r'png%7C(\S+?)%2C(\S+?)&', r.text)

        path_param = re.findall(r'path=.+?&', r.text)
        if path_param:
            out['area'] = [list(x) for x in re.findall(r'(?:%7C(-?\d+?\.\d+)%2C(-?\d+?\.\d+))', path_param[0])]
        else:
            out['area'] = list()

        markers_param = re.findall(r'markers=.+?&', r.text)
        if markers_param:
            out['point'] = re.findall(r'(-?\d+?\.\d+)%2C(-?\d+?\.\d+)', markers_param[0])
        else:
            out['point'] = []

        center_string = re.findall(r'center=.+?&', r.text)
        if center_string:
            out['center'] = re.findall(r'(-?\d+?\.\d+)%2C(-?\d+?\.\d+)', center_string[0])
        else:
            out['center'] = []

        return out

    def scrap_item_information(self, link):
        r = self.send_req(self.base + link)
        root = lxml.html.fromstring(r.content)
        with open('test2.html', 'wb') as f:
            f.write(r.content)

        information = dict()

        information['link'] = self.base + link
        information['name'] = self.get_first(root, '//h1[contains(@class, "biz-page-title")]/text()')
        information['address'] = ', '.join(filter(lambda x: x,
                                                  [x.strip() for x in
                                                   root.xpath('//div[contains(@class, "map-box-address")]//text()')]))
        information['location'] = self.get_location(r)
        information['phone'] = self.get_first(root, '//span[@class="biz-phone"]/text()')
        information['website'] = self.get_website(root)
        information['logos'] = root.xpath('//div[@class="showcase-photo-box"]/a/img/@src')
        information['reviews'] = self.get_first(root, '//span[@class="review-count rating-qualifier"]/text()')
        information['rating'] = self.get_first(root, '//div[contains(@class, "i-stars")]/@title')
        information['hours'] = self.get_hours(root)
        information['about'] = '\n'.join(filter(lambda x: x,
                                                [x.strip() for x in
                                                 root.xpath('//div[@class="from-biz-owner-content"]//text()')]))
        information['categories'] = list(filter(lambda x: x,
                                                [x.strip() for x in
                                                 root.xpath('(//span[@class="category-str-list"])[1]/a/text()')]))
        if '404 error' in r.text:
            print('404 ERROR!', link)
            information['name'] = '404 error'
            return information
        if not information['name']:
            print('Scrapping error, trying again')
            return self.scrap_item_information(link)
        else:
            print(information)
            return information

    def load_items(self, state, city):
        self.items = list()
        dump_path = os.path.join(self.dump_items_path, f'{state}_{city}.json'.replace(' ', '-'))
        if os.path.exists(dump_path):
            self.items = json.load(open(dump_path, 'r'))
        if not self.items:
            print(f'load items {state} {city}')
            for item_link in self.items_links:
                self.items.append(self.scrap_item_information(item_link))
            json.dump(self.items, open(dump_path, 'w'), indent=4)

    def start(self):
        r = requests.get('https://www.yelp.com/locations')
        root = lxml.html.fromstring(r.content)
        state_lis = root.xpath('//li[@class="state"]')

        if os.path.exists(self.locations_dump):
            locations = json.load(open(self.locations_dump, 'r'))
        else:
            locations = dict()
            for li in state_lis:
                locations[li.xpath('div/text()')[0].capitalize()] = [c for c in li.xpath('ul/li/a/text()')]
            json.dump(locations, open(self.locations_dump, 'w'), indent=4)

        for target_state, cities in locations.items():
            for target_city in cities:
                self.load_items_links(target_state, target_city)
                self.load_items(target_state, target_city)


if __name__ == '__main__':
    main()

